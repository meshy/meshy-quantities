BLUE=\x1b[34;01m
GREEN=\x1b[32;01m
NO_COLOR=\x1b[0m

help:
	@echo "Usage:"
	@echo "    $(BLUE)make help$(NO_COLOR)     -  Print this help."
	@echo "    $(BLUE)make test$(NO_COLOR)     -  Run the tests."
	@echo "    $(BLUE)make release$(NO_COLOR)  -  Release to PyPI."

test:
	@echo "$(BLUE)Running tests…$(NO_COLOR)"
	@coverage run -m unittest discover

	@echo "$(BLUE)Test coverage…$(NO_COLOR)"
	@coverage report

	@echo "$(BLUE)Testing docs…$(NO_COLOR)"
	@python -m doctest README.md

	@echo "$(BLUE)Checking code quality…$(NO_COLOR)"
	@flake8

	@echo "$(GREEN)Success!$(NO_COLOR)"

release:
	@python setup.py register sdist bdist_wheel upload
