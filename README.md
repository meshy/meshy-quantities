# meshy-quantities

A toy library for the representation of quantities. Currently, only lengths
are supported.

## Installation

```bash
pip3 install meshy-quantities
```

**Note**: This is a Python 3 only project.

## Lengths

There are three supported units of length:

- `Metre`
- `Inch`
- `Yard`

To create a length, simply instantiate the unit of your choice with a value:

```python
>>> from quantities.length import Inch
>>> Inch(6)
<Inch object of length 6>

```

If you omit the value, you will get a length one unit long.

```python
>>> Inch()
<Inch object of length 1>

```

To get the length as user-friendly string, use `str()`:

```python
>>> str(Inch(6))
'6 in'

```

To convert to another unit of measurement, use the `.to()` method:

```python
>>> from quantities.length import Inch, Metre
>>> Inch().to(Metre)
<Metre object of length 0.0254>

```

## Tests

To run the tests, first clone the project and install the test requirements:

```bash
git clone https://bitbucket.org/meshy/meshy-quantities.git
cd meshy-quantities
pip3 install -r requirements.txt
```

Once that's done, you can run the tests:
```bash
make test
```
