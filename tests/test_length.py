import itertools
from unittest import TestCase

from quantities.length import (
    Inch, INCHES_PER_YARD, Length, Metre,
    METRES_PER_INCH, METRES_PER_YARD, Yard,
)


LENGTH_SUBCLASSES = Length.__subclasses__()


class TestStringRepresentation(TestCase):
    unit_strings = {
        Metre: '42 m',
        Inch: '42 in',
        Yard: '42 yd',
    }

    def test_str(self):
        """Ensure subclasses of Length can be cast to string."""
        for unit, expected in self.unit_strings.items():
            with self.subTest(unit=unit):
                self.assertEqual(str(unit(42)), expected)

    def test_subclasses(self):
        """Ensure we've covered all subclasses of Length."""
        self.assertCountEqual(self.unit_strings.keys(), LENGTH_SUBCLASSES)


class TestDefaultLength(TestCase):
    units = [
        Metre,
        Inch,
        Yard,
    ]

    def test_default_length(self):
        """Lengths should default to being one unit long."""
        default_length = 1
        for unit in self.units:
            with self.subTest(unit=unit):
                self.assertEqual(unit().value, default_length)

    def test_subclasses(self):
        """Ensure we've covered all subclasses of Length."""
        self.assertCountEqual(self.units, LENGTH_SUBCLASSES)


class TestObjectRepresentation(TestCase):
    unit_strings = {
        Metre: '<Metre object of length 42>',
        Inch: '<Inch object of length 42>',
        Yard: '<Yard object of length 42>',
    }

    def test_repr(self):
        """Ensure Lengths have a nice raw object representation."""
        for unit, expected in self.unit_strings.items():
            with self.subTest(unit=unit):
                self.assertEqual(repr(unit(42)), expected)


class TestConversion(TestCase):
    SAMPLE_VALUE = 2
    unit_conversions = {
        # Conversion from self to self
        (Metre, Metre): SAMPLE_VALUE,
        (Inch, Inch): SAMPLE_VALUE,
        (Yard, Yard): SAMPLE_VALUE,
        # Conversion stored in table
        (Inch, Metre): SAMPLE_VALUE * METRES_PER_INCH,
        (Yard, Inch): SAMPLE_VALUE * INCHES_PER_YARD,
        (Yard, Metre): SAMPLE_VALUE * METRES_PER_YARD,
        # Inverse of conversion in table
        (Metre, Inch): SAMPLE_VALUE / METRES_PER_INCH,
        (Inch, Yard): SAMPLE_VALUE / INCHES_PER_YARD,
        (Metre, Yard): SAMPLE_VALUE / METRES_PER_YARD,
    }

    def test_conversion(self):
        """Ensure lengths can be converted into all other units."""
        for (old_unit, new_unit), expected in self.unit_conversions.items():
            with self.subTest(frm=old_unit, to=new_unit):
                new_value = old_unit(self.SAMPLE_VALUE).to(new_unit)
                self.assertEqual(new_value.value, expected)

    def test_permutations(self):
        """Ensure we've covered all conversion permutations."""
        all_combinations = itertools.product(LENGTH_SUBCLASSES, repeat=2)
        tested_combinations = self.unit_conversions.keys()
        self.assertCountEqual(all_combinations, tested_combinations)
